
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class consulta extends javax.swing.JFrame {
        
   Connection con = null;
   Statement stmt = null;
   String titulos[] = {"identificación","Nombres","Apellidos","Tipoinfo"};
   String fila[] = new String [4];
   DefaultTableModel modelo;

   
    
    public consulta() {
        initComponents();
        this.setTitle("consulta usuarios");
        this.setLocation(335,200);
        this.setResizable(false);

        try {
            
String url = "jdbc:mysql://localhost:3306/lp2_hugo";
            String usuario = "root";
            String contraseña = "003236";
            
             Class.forName("com.mysql.jdbc.Driver").newInstance(); 
             con = java.sql.DriverManager.getConnection(url,usuario,contraseña); 
                  System.out.println("Se ha establecido una conexión a la base de datos " +  
                                       "\n " + url ); 
               
               stmt = con.createStatement();
               ResultSet rs = stmt.executeQuery("select* from infor");
               
               modelo = new DefaultTableModel(null,titulos);
            
               while(rs.next()) {
                   
                   fila[0] = rs.getString("identificacion");
                   fila[1] = rs.getString("Nombres");
                   fila[2] = rs.getString("Apellidos");
                   fila[3] = rs.getString("Tipoinfo");

                   modelo.addRow(fila);     
               }
               tabla_n.setModel(modelo);
                TableColumn ci = tabla_n.getColumn("identificacion");
                ci.setMaxWidth(25);
                TableColumn cn = tabla_n.getColumn("Nombres");
                cn.setMaxWidth(165);
                TableColumn cd = tabla_n.getColumn("Apellidos");
                cd.setMaxWidth(160);
                TableColumn ct = tabla_n.getColumn("Tipoinfo");
                ct.setMaxWidth(90);
               
        }
        catch (Exception e) {
            

        }

    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabla_n = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabla_n.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabla_n);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 610, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(consulta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(consulta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(consulta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(consulta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new consulta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabla_n;
    // End of variables declaration//GEN-END:variables
}
